<?php

use Thruway\Authentication\AuthenticationManager;
use Thruway\Peer\Router;
use Thruway\Transport\InternalClientTransportProvider;
use Thruway\Transport\RatchetTransportProvider;
use WampPost\WampPost;

require_once __DIR__ . '/publisher/vendor/autoload.php';

$configs = require __DIR__ . '/publisher/config/config.php';
$websocketConfig = $configs['websocket'];

$router = new Router();

$wampPost = new WampPost(
    $websocketConfig['realm'],
    $router->getLoop(),
    sprintf('tcp://%s:%s', $websocketConfig['host'], $websocketConfig['wamp_port'])
);
$transportProvider = new RatchetTransportProvider($websocketConfig['host'], $websocketConfig['port']);

$router->addTransportProvider(new InternalClientTransportProvider($wampPost));
$router->addTransportProvider($transportProvider);
$router->registerModule(new AuthenticationManager());

$router->start();