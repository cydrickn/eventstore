CREATE TABLE `stream` (
  `stream_no` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `stream_id` VARCHAR(150) NOT NULL,
  `stream_real_name` VARCHAR(150) NOT NULL,
  `stream_name` VARCHAR(150) NOT NULL,
  `stream_metadata` JSON,
  `stream_category` VARCHAR(150),
  PRIMARY KEY (`stream_no`),
  UNIQUE KEY `ix_rsn` (`stream_real_name`),
  UNIQUE KEY `ix_id` (`stream_id`),
  KEY `ix_cat` (`stream_category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
