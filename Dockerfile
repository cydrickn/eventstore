FROM alpine:latest

RUN apk add --update gettext

COPY ./.docker/code/manifest /
COPY ./ /app

RUN adduser -D -g 'www' www

RUN chown -R www:www /app

USER www
WORKDIR /app
VOLUME /app

CMD ["/bin/sh", "/entrypoint.sh"]