# Event Store
An event store is a type of database optimized for storage of events.

## Introduction
This project is created using PHP with MySQL as Database.
The use of this just an storage of all your events, specifically your domain events.

The whole thing was design as micro service where writing and reading from the event store
are separated

*/services/read* The read service
*/services/write* The write service

### Installation using Docker

Clone the repository
```
$ git clone https://github.com/cydrickn/event-store.git && cd ./event-store
```

Run docker-compose
```
$ docker-compose up -d
```

Import the initial event stream
```
docker-compose exec -T store mysql -u root -p"root" event_store < ./scripts/01_event_streams_table.sql
```

Install dependencies
```
$ docker-compose exec read cd /app/service && composer install
$ docker-compose exec write cd /app/service && composer install
```

Now access `http://<your-ip>`

Upon going to url `http://<yout-ip>`. You will have a swagger,
in *host* field input your ip.

And thats it.