docker() {
    if [ ! -e ~/.docker/config.json ]; then
        if [ -z ${CI_REGISTRY_INSECURE+x} ]; then
            command docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
        else
            command docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY_INSECURE
        fi
    fi
    command docker "$@"
}

build_code () {
    echo "Building Code Repo"
    apk add --update php7 \
        php7-cli \
        php7-curl \
        php7-openssl \
        php7-json \
        php7-fpm \
        php7-pdo \
        php7-mysqli \
        php7-mbstring \
        php7-gd \
        php7-dom \
        php7-xml \
        php7-posix \
        php7-intl \
        php7-apcu \
        php7-phar \
        php7-zlib \
        php7-fileinfo \
        php7-simplexml \
        php7-tokenizer \
        php7-xmlwriter \
        php7-bz2 \
        php7-ctype \
        php7-session \
        php7-pdo_mysql \
        acl \
        gettext \
        curl \
        zip
    
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    php composer-setup.php --filename=composer --install-dir=/usr/bin
    rm composer-setup.php
    chmod +x /usr/bin/composer
    
    composer install --no-scripts
    
    cd ./bin/publisher
    composer install --no-scripts
}