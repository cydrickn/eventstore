<?php

/**
 * This file is part of the cydrickn/event-store library
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Cydrick Nonog <cydrick.dev@gmail.com>
 * @license http://opensource.org/licenses/MIT MIT
 * @link https://github.com/cydrickn/event-store GitHub
 */

declare(strict_types=1);

namespace Cydrickn\EventStore\Presentation\Controller;

use Assert\InvalidArgumentException;
use Cydrickn\EventStore\Application\Command\AddStreamEventCommand;
use Cydrickn\EventStore\Application\Command\CreateStreamCommand;
use Cydrickn\EventStore\Infrastructure\EventStore\EventStoreInterface;
use Cydrickn\EventStore\Infrastructure\Exceptions\EventAlreadyExistsException;
use Cydrickn\EventStore\Infrastructure\Response\Response as WriteResponse;
use Cydrickn\EventStore\Infrastructure\Service\WriteService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Description of WriteController
 *
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
class WriteController extends Controller
{
    public function createStream(Request $request): Response
    {
        $bodyContent = $request->getContent();
        $command = CreateStreamCommand::fromJsonString($bodyContent);
        $this->getWriteService()->handleCreateStreamCommand($command);

        return Response::create('', Response::HTTP_CREATED, [
            'Location' => $request->getScheme() . '://' . $request->getHost() . '/' . $command->streamName()->toString(),
        ]);
    }

    public function createEvent(Request $request, string $streamName): Response
    {
        $bodyContent = $request->getContent();
        $payload = json_decode($bodyContent, true);
        $payload['stream'] = $streamName;

        $response = [];
        try {
            $command = new AddStreamEventCommand($payload);
            $this->getWriteService()->handleAddStreamEventCommand($command);
        } catch (InvalidArgumentException $e) {
            return JsonResponse::create(['error' => $e->getMessage()], Response::HTTP_UNPROCESSABLE_ENTITY);
        } catch (EventAlreadyExistsException $e) {
            return JsonResponse::create(['error' => $e->getMessage()], Response::HTTP_UNPROCESSABLE_ENTITY);
        } catch (WriteResponse $e) {
            $response = ['stream' => ['name' => $streamName], 'events' => $e->getContent()];
        }

        return JsonResponse::create($response, Response::HTTP_CREATED);
    }

    private function getWriteService(): WriteService
    {
        return $this->get('write_service');
    }

    private function getEventStore(): EventStoreInterface
    {
        return $this->get('event_store');
    }
}
