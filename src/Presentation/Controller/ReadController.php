<?php

/**
 * This file is part of the cydrickn/event-store library
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Cydrick Nonog <cydrick.dev@gmail.com>
 * @license http://opensource.org/licenses/MIT MIT
 * @link https://github.com/cydrickn/event-store GitHub
 */

declare(strict_types=1);

namespace Cydrickn\EventStore\Presentation\Controller;

use Cydrickn\EventStore\Domain\Event\EventId;
use Cydrickn\EventStore\Domain\Stream\NullStream;
use Cydrickn\EventStore\Domain\Stream\StreamName;
use Cydrickn\EventStore\Infrastructure\EventStore\EventStoreInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Description of ReadController
 *
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
class ReadController extends BaseController
{
    public function swagger(): Response
    {
        $swaggerDoc = Yaml::parseFile($this->container->get('kernel')->getConfigDir() . '/swagger.yml');

        return $this->json($swaggerDoc, JsonResponse::HTTP_OK);

    }
    
    public function readStream(Request $request, string $streamName): Response
    {
        $stream = $this->getEventStore()->fetchStream(StreamName::fromString($streamName));
        $limit = (int) $request->get('limit', 20);
        $offset = (int) $request->get('offset', 0);
        $criteria = $request->get('criteria', []);
        
        if ($stream instanceof NullStream) {
            return JsonResponse::create([], Response::HTTP_NOT_FOUND);
        }
        $events = $this->getEventStore()->load($stream, [], $limit, $offset);
        $result = [
            'stream_id' => $stream->id()->toString(),
            'stream_name' => $stream->realName()->toString(),
            'stream_metadata' => $stream->metaData()->toArray(),
            'stream_category' => $stream->category()->toString(),
            'request_info' => [
                'limit' => $limit,
                'offset' => $offset,
                'criteria' => $criteria,
            ],
            'links' => [
                [
                    'uri' => $this->generateUrl(
                        'read_stream',
                        ['streamName' => $streamName, 'limit' => $limit, 'offset' => $offset],
                        UrlGeneratorInterface::ABSOLUTE_URL
                    ),
                    'relation' => 'self',
                ],
                [
                    'uri' => $this->generateUrl(
                        'read_stream',
                        ['streamName' => $streamName, 'limit' => $limit, 'offset' => $offset + $limit],
                        UrlGeneratorInterface::ABSOLUTE_URL
                    ),
                    'relation' => 'next',
                ],
            ],
            'events' => []
        ];
        if ($offset > 0) {
            $result['links'][] = [
                'uri' => $this->generateUrl(
                    'read_stream',
                    ['streamName' => $streamName, 'limit' => $limit, 'offset' => $offset - $limit],
                    UrlGeneratorInterface::ABSOLUTE_URL
                ),
                'relation' => 'previous',
            ];
        }

        foreach ($events as $event) {
            $result['events'][] = [
                'event_id' => $event->id()->toString(),
                'event_name' => $event->eventName()->toString(),
                'event_data' => $event->data()->toArray(),
                'event_metadata' => $event->metaData()->toArray(),
                'event_occured_at' => $event->occuredAtAsString(),
                'links' => [
                    [
                        'uri' => $this->generateUrl(
                            'read_event_inposition',
                            ['streamName' => $streamName, 'position' => $event->metaData()->get('_position')],
                            UrlGeneratorInterface::ABSOLUTE_URL
                        ),
                        'relation' => 'self'
                    ],
                    [
                        'uri' => $this->generateUrl(
                            'read_event_by_event_id',
                            ['streamName' => $streamName, 'eventId' => $event->eventId()->toString()],
                            UrlGeneratorInterface::ABSOLUTE_URL
                        ),
                        'relation' => 'alternate'
                    ]
                ]
            ];
        }

        return JsonResponse::create($result);
    }

    public function readEventInPosition(Request $request, string $streamName, int $position): Response
    {
        $stream = $this->getEventStore()->fetchStream(StreamName::fromString($streamName));
        if ($stream instanceof NullStream) {
            return Response::create('', Response::HTTP_NOT_FOUND);
        }

        $event = $this->getEventStore()->loadFromPosition($stream, $position);
        $result = [
            'event_id' => $event->id()->toString(),
            'event_name' => $event->eventName()->toString(),
            'event_data' => $event->data()->toArray(),
            'event_metadata' => $event->metaData()->toArray(),
            'event_occured_at' => $event->occuredAtAsString(),
            'links' => [
                [
                    'uri' => $this->generateUrl(
                        'read_event_inposition',
                        ['streamName' => $streamName, 'position' => $event->metaData()->get('_position')],
                        UrlGeneratorInterface::ABSOLUTE_URL
                    ),
                    'relation' => 'self'
                ],
                [
                    'uri' => $this->generateUrl(
                        'read_event_by_event_id',
                        ['streamName' => $streamName, 'eventId' => $event->eventId()->toString()],
                        UrlGeneratorInterface::ABSOLUTE_URL
                    ),
                    'relation' => 'alternate'
                ]
            ],
        ];

        return JsonResponse::create($result);
    }
    
    public function readEventByEventId(Request $request, string $streamName, string $eventId): Response
    {
        $stream = $this->getEventStore()->fetchStream(StreamName::fromString($streamName));
        if ($stream instanceof NullStream) {
            return Response::create('', Response::HTTP_NOT_FOUND);
        }
        
        $event = $this->getEventStore()->loadEvent($stream, EventId::fromString($eventId));
        $result = [
            'event_id' => $event->id()->toString(),
            'event_name' => $event->eventName()->toString(),
            'event_data' => $event->data()->toArray(),
            'event_metadata' => $event->metaData()->toArray(),
            'event_occured_at' => $event->occuredAtAsString(),
            'links' => [
                [
                    'uri' => $this->generateUrl(
                        'read_event_by_event_id',
                        ['streamName' => $streamName, 'eventId' => $event->eventId()->toString()],
                        UrlGeneratorInterface::ABSOLUTE_URL
                    ),
                    'relation' => 'self'
                ],
                [
                    'uri' => $this->generateUrl(
                        'read_event_inposition',
                        ['streamName' => $streamName, 'position' => $event->metaData()->get('_position')],
                        UrlGeneratorInterface::ABSOLUTE_URL
                    ),
                    'relation' => 'alternate'
                ],
            ],
        ];

        return JsonResponse::create($result);
    }

    public function readStreams(Request $request): Response
    {
        $streamNames = $this->getEventStore()->fetchStreamNames();

        return JsonResponse::create($streamNames);
    }

    public function getEventStore(): EventStoreInterface
    {
        return $this->get('event_store');
    }
}
