<?php

/**
 * This file is part of the cydrickn/event-store library
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Cydrick Nonog <cydrick.dev@gmail.com>
 * @license http://opensource.org/licenses/MIT MIT
 * @link https://github.com/cydrickn/event-store GitHub
 */

declare(strict_types=1);

namespace Cydrickn\EventStore\Infrastructure\EventStore;

use Cydrickn\EventStore\Domain\Common\MetaData;
use Cydrickn\EventStore\Domain\Event\EventId;
use Cydrickn\EventStore\Domain\Event\EventInterface;
use Cydrickn\EventStore\Domain\Stream\Stream;
use Cydrickn\EventStore\Domain\Stream\StreamInterface;
use Cydrickn\EventStore\Domain\Stream\StreamName;
use Iterator;

/**
 *
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
interface ReadOnlyEventStoreInterface
{
    public function fetchStream(StreamName $streamName): StreamInterface;
    public function hasStream(StreamName $streamName): bool;
    public function load(Stream $stream, array $criteria = [], int $limit = 100, int $offset = 0): Iterator;
    public function loadEvent(Stream $stream, EventId $eventId): EventInterface;
    public function loadFromPosition(Stream $stream, int $position): EventInterface;
    public function loadFromVersion(Stream $stream, int $version, MetaData $metaData): EventInterface;
    public function fetchStreamNames(array $filters = [], int $limit = 20, int $offset = 0): array;
    public function fetchCategoryNames(array $filters, int $limit = 20, int $offset = 0): array;
    public function getCurrentVersion(Stream $stream, MetaData $metaData): int;
}
