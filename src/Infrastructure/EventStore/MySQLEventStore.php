<?php

/**
 * This file is part of the cydrickn/event-store library
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Cydrick Nonog <cydrick.dev@gmail.com>
 * @license http://opensource.org/licenses/MIT MIT
 * @link https://github.com/cydrickn/event-store GitHub
 */

declare(strict_types=1);

namespace Cydrickn\EventStore\Infrastructure\EventStore;

use Cydrickn\EventStore\Infrastructure\EventStore\EventStoreInterface;
use Cydrickn\EventStore\Infrastructure\Exceptions\EventAlreadyExistsException;
use Cydrickn\EventStore\Domain\Common\MetaData;
use Cydrickn\EventStore\Domain\Event\Event;
use Cydrickn\EventStore\Domain\Event\EventId;
use Cydrickn\EventStore\Domain\Event\EventInterface;
use Cydrickn\EventStore\Domain\Event\EventIterator;
use Cydrickn\EventStore\Domain\Event\NullEvent;
use Cydrickn\EventStore\Domain\Stream\NullStream;
use Cydrickn\EventStore\Domain\Stream\Stream;
use Cydrickn\EventStore\Domain\Stream\StreamInterface;
use Cydrickn\EventStore\Domain\Stream\StreamName;
use Cydrickn\EventStore\Infrastructure\EventStore\PersistenceStrategy\PersistenceStrategyCollection;
use Cydrickn\EventStore\Infrastructure\EventStore\PersistenceStrategy\PersistenceStrategyInterface;
use Exception;
use Iterator;
use PDO;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Description of MySQLEventStore
 *
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
class MySQLEventStore implements EventStoreInterface
{
    use ContainerAwareTrait;

    const EVENT_STREAM_TABLE = 'stream';

    private $connection;
    private $strategyCollection;

    public function __construct(PDO $connection, PersistenceStrategyCollection $strategyCollection)
    {
        $this->connection = $connection;
        $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->strategyCollection = $strategyCollection;
    }

    public function appendTo(Stream $stream, Iterator $events): void
    {
        foreach ($events as $event) {
            if ($event instanceof Event && $this->eventAlreadyExists($stream, $event->id(), $event->metaData())) {
                throw new EventAlreadyExistsException(sprintf('Event already exists: %s', $event->id()->toString()));
            }
        }

        $events->rewind();
        $persistenceStrategy = $this->getPersistenceStrategyFromMetaData($stream->metaData());
        $data = $persistenceStrategy->prepareData($events);
        $columns = $persistenceStrategy->columnNames();

        $places = '(' . implode(', ', array_fill(0, count($columns), '?')) . ')';
        $sql = sprintf('INSERT INTO %s (%s) VALUES %s', $stream->name()->toString(), implode(',', $columns), $places);

        try {
            $this->connection->beginTransaction();
            foreach ($events as $key => $event) {
                $eventData = $data[$key];

                $stmt = $this->connection->prepare($sql);
                $stmt->execute($eventData);

                $event->setPosition((int) $this->connection->lastInsertId());
                $event->setVersion((int) $persistenceStrategy->getVersion($event));

                $stmt->closeCursor();
            }
            $this->connection->commit();
        } catch (Exception $ex) {
            $this->connection->rollBack();
            throw $ex;
        }
    }

    public function create(Stream $stream): void
    {
        $this->addStreamToStreamTable($stream);
        $this->createSchemeFor($stream);
    }

    public function fetchCategoryNames(array $filters, int $limit = 20, int $offset = 0): array
    {
        $sql = 'SELECT stream_category FROM ' . self::EVENT_STREAM_TABLE
            . ' GROUP BY stream_category ORDER BY stream_category ASC LIMIT ' . $offset . ', '. $limit;
        $stmt = $this->connection->query($sql);
        $stmt->execute();
        $categories = [];

        foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $category) {
            $categories[] = $category['stream_category'];
        }

        $stmt->closeCursor();
        unset($stmt);

        return $categories;
    }

    public function fetchStream(StreamName $streamName): StreamInterface
    {
        $stream = new NullStream();
        $sqlFetch = 'SELECT * FROM stream WHERE stream_real_name = ? LIMIT 1';
        $fetchQuery = $this->connection->prepare($sqlFetch);
        $fetchQuery->execute([$streamName->toString()]);
        if ($fetchQuery->rowCount() === 1) {
            $fetchData = $fetchQuery->fetch(PDO::FETCH_SERIALIZE);
            $fetchData['stream_metadata'] = json_decode($fetchData['stream_metadata'], true);
            $stream = Stream::fromArray($fetchData);
        }
        $fetchQuery->closeCursor();
        $fetchQuery = null;
        unset($fetchQuery);

        return $stream;
    }

    public function fetchStreamNames(array $filters = array(), int $limit = 20, int $offset = 0): array
    {
        $streamNames = [];

        $sql = 'SELECT stream_real_name FROM ' . self::EVENT_STREAM_TABLE
            . ' GROUP BY stream_real_name ORDER BY stream_real_name ASC LIMIT ' . $offset . ', ' . $limit;
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();

        foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $stream) {
            $streamNames[] = $stream['stream_real_name'];
        }

        $stmt->closeCursor();
        unset($stmt);

        return $streamNames;
    }

    public function hasStream(StreamName $streamName): bool
    {
        $sql = 'SELECT COUNT(*) as totalStream FROM ' . self::EVENT_STREAM_TABLE . ' WHERE stream_real_name = ?';
        $stmt = $this->connection->prepare($sql);
        $stmt->execute([$streamName->toString()]);
        $totalStream = (int) $stmt->fetchColumn();
        $stmt->closeCursor();
        unset($stmt);

        return $totalStream > 0;
    }

    public function load(
        Stream $stream,
        array $criteria = [],
        int $limit = 100,
        int $offset = 0
    ): Iterator {
        $tableName = $stream->name()->toString();
        $persistenceStrategy = $this->getPersistenceStrategyFromMetaData($stream->metaData());
        $wheres = [];
        $parameters = [];
        foreach ($criteria as $condition) {
            $generatedCondition = $this->generateFilters($condition);
            $wheres = array_merge($wheres, $generatedCondition['where']);
            $parameters = array_merge($parameters, $generatedCondition['parameters']);
        }
        $whereAsString = implode(' AND ', $wheres);
        if (!empty($wheres)) {
            $whereAsString = 'WHERE ' . $whereAsString;
        }
        $sql = sprintf(
            'SELECT * FROM %s %s ORDER BY event_no ASC LIMIT %s, %s',
            $tableName,
            $whereAsString,
            $offset,
            $limit
        );
        $events = [];

        $stmt = $this->connection->query($sql);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $stmt->execute($parameters);

        foreach ($stmt as $event) {
            $dataEvent = $event;
            $dataEvent['event_data'] = json_decode($event['event_data'], true);
            $dataEvent['event_metadata'] = json_decode($event['event_metadata'], true);
            $dataEvent['event_metadata']['_position'] = $event['event_no'];
            $eventObject = Event::fromArray($dataEvent);
            $eventObject->setVersion($persistenceStrategy->getVersion($eventObject));
            $events[] = $eventObject;
        }

        $stmt->closeCursor();

        return new EventIterator($events);
    }

    public function updateStreamMetaData(StreamName $streamName, MetaData $metaData): void
    {
        $sql = 'UPDATE ' . self::EVENT_STREAM_TABLE . ' SET stream_metadata = ? WHERE stream_real_name = ?';
        $stmt = $this->connection->prepare($sql);
        $stmt->execute([$metaData->toJson(), $streamName->toString()]);
        $stmt->closeCursor();
    }

    public function getPersistenceStrategyFromMetaData(MetaData $metaData): PersistenceStrategyInterface
    {
        $strategyName = 'simple';
        if ($metaData->has('persistence_strategy')) {
            $strategyName = $$metaData->get('persistence_strategy');
        }

        return $this->strategyCollection->get($strategyName);
    }

    public function getCurrentVersion(Stream $stream, MetaData $metaData): int
    {
        $strategy = $this->getPersistenceStrategyFromMetaData($stream->metaData());
        $command = $strategy->generateFetchCurrentVersion($stream->name()->toString(), $metaData);

        $stmt = $this->connection->query($command['command']);
        $stmt->execute($command['params'] ?? []);
        $version = $stmt->fetchColumn();
        if ($version === false) {
            $version = 0;
        }
        $stmt->closeCursor();
        unset($stmt);

        return (int) $version;
    }

    private function addStreamToStreamTable(Stream $stream): void
    {
        $sql = sprintf('INSERT INTO %s (stream_id, stream_real_name, stream_name, stream_metadata, stream_category)'
            . ' VALUES (?, ?, ?, ?, ?)', self::EVENT_STREAM_TABLE);

        $stmt = $this->connection->prepare($sql);
        $stmt->execute([
            $stream->id()->toString(),
            $stream->realName()->toString(),
            $stream->name()->toString(),
            $stream->metaData()->toJson(),
            $stream->category()->toString(),
        ]);
        $stmt->closeCursor();
        $stmt = null;
        unset($stmt);
    }

    private function createSchemeFor(Stream $stream): void
    {
        $schema = $this->getPersistenceStrategyFromMetaData($stream->metaData())->createSchema($stream->name()->toString());
        foreach ($schema as $command) {
            $this->connection->exec($command);
        }
    }

    private function generateFilters(array $condition): array
    {
        $fieldType = $condition['field_type'];
        $value = $condition['value'];
        $field = $condition['field'];
        $operator = $condition['operator'];
        $parameters = [];

        if (is_array($value)) {
            foreach ($value as $val) {
                $parameters[] = $val;
            }
        } else {
            $parameters[] = $value;
        }

        $operatorStart = '';
        $operatorEnd = '';
        if ($operator === 'in') {
            $operatorStart = $field . ' IN (';
            $operatorEnd = ')';
        } elseif ($operator === 'not in') {
            $operatorStart = $field . ' NOT IN (';
            $operatorEnd = ')';
        } else {
            $operatorStart = $field . ' ' . $operator;
        }

        $where = $operatorStart . implode(', ', array_fill(0, count($parameters), '?')) . $operatorEnd;

        return ['where' => $where, 'parameters' => $parameters];
    }

    public function loadEvent(Stream $stream, EventId $eventId): EventInterface
    {
        $table = $stream->name()->toString();
        $sql = sprintf('SELECT * FROM %s WHERE event_id = ? LIMIT 1', $table);
        $stmt = $this->connection->prepare($sql);
        $stmt->execute([$eventId->toString()]);
        $record = $stmt->fetch(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        unset($stmt);

        if ($record === false) {
            $event = new NullEvent();
        } else {
            $dataEvent = $record;
            $dataEvent['event_data'] = json_decode($record['event_data'], true);
            $dataEvent['event_metadata'] = json_decode($record['event_metadata'], true);
            $dataEvent['event_metadata']['_position'] = $record['event_no'];
            $event = Event::fromArray($dataEvent);
        }

        return $event;
    }

    public function loadFromPosition(Stream $stream, int $position): EventInterface
    {
        $table = $stream->name()->toString();
        $sql = sprintf('SELECT * FROM %s WHERE event_no = ? LIMIT 1', $table);
        $stmt = $this->connection->prepare($sql);
        $stmt->execute([$position]);
        $record = $stmt->fetch(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        unset($stmt);

        if ($record === false) {
            $event = new NullEvent();
        } else {
            $dataEvent = $record;
            $dataEvent['event_data'] = json_decode($record['event_data'], true);
            $dataEvent['event_metadata'] = json_decode($record['event_metadata'], true);
            $dataEvent['event_metadata']['_position'] = $record['event_no'];
            $event = Event::fromArray($dataEvent);
        }

        return $event;
    }

    public function loadFromVersion(Stream $stream, int $version, MetaData $metaData): EventInterface
    {
        $command = $this->getPersistenceStrategyFromMetaData($stream->metaData())
            ->generateFetchByVersion($stream->name()->toString(), $version, $metaData);

        $stmt = $this->connection->prepare($command['command']);
        $stmt->execute($command['params']);
        $record = $stmt->fetch(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        unset($stmt);

        if ($record === false) {
            $event = new NullEvent();
        } else {
            $dataEvent = $record;
            $dataEvent['event_data'] = json_decode($record['event_data'], true);
            $dataEvent['event_metadata'] = json_decode($record['event_metadata'], true);
            $dataEvent['event_metadata']['_position'] = $record['event_no'];
            $event = Event::fromArray($dataEvent);
        }

        return $event;
    }

    public function eventAlreadyExists(Stream $stream, EventId $eventId, MetaData $metaData): bool
    {
        $command = $this->getPersistenceStrategyFromMetaData($stream->metaData())
            ->generateCheckIntegrety($stream->name()->toString(), $eventId, $metaData);

        $stmt = $this->connection->prepare($command['command']);
        $stmt->execute($command['params']);
        $exists = $stmt->rowCount() > 0;
        $stmt->closeCursor();
        unset($stmt);

        return $exists;
    }
}
