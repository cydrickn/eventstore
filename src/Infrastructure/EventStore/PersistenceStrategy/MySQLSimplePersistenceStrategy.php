<?php

/**
 * This file is part of the cydrickn/event-store library
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Cydrick Nonog <cydrick.dev@gmail.com>
 * @license http://opensource.org/licenses/MIT MIT
 * @link https://github.com/cydrickn/event-store GitHub
 */
declare(strict_types=1);

namespace Cydrickn\EventStore\Infrastructure\EventStore\PersistenceStrategy;

use Cydrickn\EventStore\Domain\Common\MetaData;
use Cydrickn\EventStore\Domain\Event\Event;
use Cydrickn\EventStore\Domain\Event\EventId;
use Cydrickn\EventStore\Domain\Stream\StreamId;
use Cydrickn\EventStore\Domain\Stream\StreamName;
use DateTimeZone;
use Iterator;
use RuntimeException;

/**
 * Description of MySQLPersistenceStrategy
 *
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
class MySQLSimplePersistenceStrategy implements PersistenceStrategyInterface
{
    public function columnNames(): array
    {
        return [
            'event_id',
            'event_name',
            'event_data',
            'event_metadata',
            'event_occured_at'
        ];
    }

    public function createSchema(string $tableName): array
    {
        $createSql = 'CREATE TABLE `' . $tableName . '` ('
            . '`event_no` BIGINT NOT NULL AUTO_INCREMENT'
            . ', `event_id` CHAR(36) COLLATE utf8_bin NOT NULL'
            . ', `event_name` VARCHAR(100) COLLATE utf8_bin NOT NULL'
            . ', `event_data` JSON NOT NULL'
            . ', `event_metadata` JSON NOT NULL'
            . ', `event_occured_at` TIMESTAMP NOT NULL'
            . ", `event_correlation_id` CHAR(36) CHARACTER SET utf8 COLLATE utf8_bin GENERATED ALWAYS AS"
            . " (JSON_UNQUOTE(JSON_EXTRACT(event_metadata, '$.correlation_id')))"
            . ", `event_causation_id` CHAR(36) CHARACTER SET utf8 COLLATE utf8_bin GENERATED ALWAYS AS"
            . " (JSON_UNQUOTE(JSON_EXTRACT(event_metadata, '$.causation_id')))"
            . ', PRIMARY KEY (`event_no`)'
            . ', UNIQUE KEY `ix_event_id` (`event_id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin';

        return [$createSql];
    }

    public function generateTableName(StreamId $streamId, StreamName $streamName): string
    {
        return $tableName = '_' . sha1($streamId->toString());
    }

    public function prepareData(Iterator $streamEvents): array
    {
        $events = [];

        foreach ($streamEvents as $event) {
            if ($event instanceof Event) {
                $eventOccuredAt = $event->occuredAt()->setTimezone(new DateTimeZone('UTC'));
                $events[] = [
                    $event->id()->toString(),
                    $event->name()->toString(),
                    $event->metaData()->toJson(),
                    $event->data()->toJson(),
                    $eventOccuredAt->format('Y-m-d H:i:s'),
                ];
            } else {
                throw RuntimeException('Event must be an ' . Event::class);
            }
        }

        return $events;
    }

    public function generateFetchCurrentVersion(string $table, MetaData $metaData): array
    {
        return ['command' => "SELECT event_no FROM " . $table . ' ORDER BY event_no DESC LIMIT 1'];
    }

    public function generateFetchByVersion(string $table, int $version, MetaData $metaData): array
    {
        return[
            'command' => 'SELECT * FROM ' . $table . ' WHERE event_no = ?',
            'params' => [$version],
        ];
    }

    public function generateCheckIntegrety(string $table, EventId $eventId, MetaData $metaData): array
    {
        return [
            'command' => 'SELECT * FROM ' . $table .' WHERE event_id = ?',
            'params' => [$eventId->toString()]
        ];
    }

    public function getVersion(Event $event): int
    {
        return (int) $event->metaData()->get('_position');
    }
}
