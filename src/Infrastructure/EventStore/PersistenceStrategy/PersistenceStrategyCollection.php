<?php

/**
 * This file is part of the cydrickn/event-store library
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Cydrick Nonog <cydrick.dev@gmail.com>
 * @license http://opensource.org/licenses/MIT MIT
 * @link https://github.com/cydrickn/event-store GitHub
 */
declare(strict_types=1);

namespace Cydrickn\EventStore\Infrastructure\EventStore\PersistenceStrategy;

/**
 * Description of EventStoreStrategyService
 *
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
class PersistenceStrategyCollection
{
    private $strategy = [];

    public function add($referrence, string $strategy): void
    {
        $this->strategy[$strategy] = $referrence;
    }

    public function get(string $strategy): PersistenceStrategyInterface
    {
        return $this->strategy[$strategy];
    }

    public function has(string $strategy): bool
    {
        return isset($this->strategy[$strategy]);
    }
}
