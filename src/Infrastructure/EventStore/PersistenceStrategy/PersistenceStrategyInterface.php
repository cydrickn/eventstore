<?php

/**
 * This file is part of the cydrickn/event-store library
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Cydrick Nonog <cydrick.dev@gmail.com>
 * @license http://opensource.org/licenses/MIT MIT
 * @link https://github.com/cydrickn/event-store GitHub
 */

declare(strict_types=1);

namespace Cydrickn\EventStore\Infrastructure\EventStore\PersistenceStrategy;

use Cydrickn\EventStore\Domain\Common\MetaData;
use Cydrickn\EventStore\Domain\Event\Event;
use Cydrickn\EventStore\Domain\Event\EventId;
use Cydrickn\EventStore\Domain\Stream\StreamId;
use Cydrickn\EventStore\Domain\Stream\StreamName;
use Iterator;

/**
 *
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
interface PersistenceStrategyInterface
{
    public function createSchema(string $tableName): array;

    public function columnNames(): array;

    public function prepareData(Iterator $streamEvents): array;

    public function generateTableName(StreamId $streamId, StreamName $streamName): string;

    public function generateFetchCurrentVersion(string $table, MetaData $metaData): array;

    public function generateFetchByVersion(string $table, int $version, MetaData $metaData): array;

    public function generateCheckIntegrety(string $table, EventId $eventId, MetaData $metaData): array;

    public function getVersion(Event $event): int;
}
