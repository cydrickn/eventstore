<?php

/**
 * This file is part of the cydrickn/event-store library
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Cydrick Nonog <cydrick.dev@gmail.com>
 * @license http://opensource.org/licenses/MIT MIT
 * @link https://github.com/cydrickn/event-store GitHub
 */

declare(strict_types=1);

namespace Cydrickn\EventStore\Infrastructure\EventStore;

use Cydrickn\EventStore\Domain\Common\MetaData;
use Cydrickn\EventStore\Domain\Event\EventId;
use Cydrickn\EventStore\Domain\Stream\Stream;
use Cydrickn\EventStore\Domain\Stream\StreamName;
use Cydrickn\EventStore\Infrastructure\EventStore\PersistenceStrategy\PersistenceStrategyInterface;
use Iterator;

/**
 * Description of EventStore
 *
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
interface EventStoreInterface extends ReadOnlyEventStoreInterface
{
    public function updateStreamMetaData(StreamName $streamName, MetaData $metaData): void;
    public function create(Stream $stream): void;
    public function appendTo(Stream $stream, Iterator $events): void;
    public function getPersistenceStrategyFromMetaData(MetaData $metaData): PersistenceStrategyInterface;
    public function eventAlreadyExists(Stream $stream, EventId $eventId, MetaData $metaData): bool;
}
