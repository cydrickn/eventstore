<?php

/**
 * This file is part of the cydrickn/event-store library
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Cydrick Nonog <cydrick.dev@gmail.com>
 * @license http://opensource.org/licenses/MIT MIT
 * @link https://github.com/cydrickn/event-store GitHub
 */
declare(strict_types=1);

namespace Cydrickn\EventStore\Infrastructure\Response;

/**
 * Description of HttpResponse
 *
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
class Response extends \Exception
{
    private $content;

    public function setContent($content): void
    {
        $this->content = $content;
    }

    public function getContent()
    {
        return $this->content;
    }
}
