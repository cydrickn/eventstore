<?php

/**
 * This file is part of the cydrickn/event-store library
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Cydrick Nonog <cydrick.dev@gmail.com>
 * @license http://opensource.org/licenses/MIT MIT
 * @link https://github.com/cydrickn/event-store GitHub
 */
declare(strict_types=1);

namespace Cydrickn\EventStore\Infrastructure\Utils;

/**
 * Description of Criteria
 *
 * @author cydrick
 */
class Criteria
{
    private $criteria;
    private $conditions;
    
    
    public function __construct(array $criteria)
    {
        $this->criteria = $criteria;
        $this->generateConditions();
    }
    
    public function toArray(): array
    {
        return $this->conditions;
    }
}
