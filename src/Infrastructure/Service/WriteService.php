<?php

/**
 * This file is part of the cydrickn/event-store library
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Cydrick Nonog <cydrick.dev@gmail.com>
 * @license http://opensource.org/licenses/MIT MIT
 * @link https://github.com/cydrickn/event-store GitHub
 */
declare(strict_types=1);

namespace Cydrickn\EventStore\Infrastructure\Service;

use Cydrickn\EventStore\Domain\Event\EventIterator;
use Cydrickn\EventStore\Domain\Stream\Stream;
use Cydrickn\EventStore\Domain\Stream\StreamId;
use Cydrickn\EventStore\Application\Command\AddStreamEventCommand;
use Cydrickn\EventStore\Application\Command\CreateStreamCommand;
use Cydrickn\EventStore\Infrastructure\EventStore\EventStoreInterface;
use Cydrickn\EventStore\Infrastructure\Exceptions\WrongExpectedVersionException;
use Cydrickn\EventStore\Infrastructure\Response\Response;
use Ramsey\Uuid\Uuid;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Description of WriteSservice
 *
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
class WriteService
{
    use ContainerAwareTrait;

    const VERSION_JUST_APPEND = -1;
    const VERSION_NO_EVENTS = 0;
    const VERSION_MUST_HAVE_EVENTS = -2;

    private $eventStore;

    public function __construct(EventStoreInterface $eventStore)
    {
        $this->eventStore = $eventStore;
    }

    public function handleCreateStreamCommand(CreateStreamCommand $command): void
    {
        $data = $command->toArray();
        $data['stream_real_name'] = $command->streamName()->toString();
        $data['stream_name'] = $this->eventStore
            ->getPersistenceStrategyFromMetaData($command->streamMetaData())
            ->generateTableName($command->streamId(), $command->streamName());

        $stream = Stream::fromArray($data);
        $this->eventStore->create($stream);
    }

    public function handleAddStreamEventCommand(AddStreamEventCommand $command): void
    {
        $streamName = $command->streamName();
        if (!$this->eventStore->hasStream($streamName)) {
            $streamId = Uuid::uuid4()->toString();
            $streamTable = $this
                ->eventStore
                ->getPersistenceStrategyFromMetaData($command->metaData())
                ->generateTableName(StreamId::fromString($streamId), $streamName);
            $stream = Stream::fromArray([
                'stream_id' => $streamId,
                'stream_real_name' => $streamName->toString(),
                'stream_name' => $streamTable,
                'stream_metadata' => $command->metaData()->toArray(),
                'stream_category' => '',
            ]);

            $this->eventStore->create($stream);
        } else {
            $stream = $this->eventStore->fetchStream($streamName);
        }

        $currentVersion = $this->eventStore->getCurrentVersion($stream, $command->metaData());
        $continueAppending = true;
        switch ($command->expectedVersion()) {
            case self::VERSION_JUST_APPEND:
                break;
            case self::VERSION_MUST_HAVE_EVENTS:
                $continueAppending = $currentVersion > 0;
                break;
            case self::VERSION_NO_EVENTS:
                $continueAppending = $currentVersion === 0;
                break;
            default:
                $continueAppending = $currentVersion === $command->expectedVersion();
                break;
        }
        if ($continueAppending) {
            $events = $command->events();
            $this->eventStore->appendTo($stream, $events);
            $this->publishEvent($stream, $events);

            $response = new Response();
            $response->setContent($events->toArray());

            throw $response;
        } else {
            throw new WrongExpectedVersionException((string) $currentVersion);
        }
    }

    public function publishEvent(Stream $stream, EventIterator $events): void
    {
        $configs = $this->container->getParameter('publisher');
        $path = sprintf('%s://%s:%s/pub', $configs['scheme'], $configs['host'], $configs['port']);
        $eventsArray = [];
        foreach ($events as $event) {
            $eventsArray[] = $event->serialize();
        }
        $eventsAsJson = json_encode($this->generatePublisherData($stream, $events));

        $commnad = [
            'curl -H "Content-Type: application/json"',
            '-d',
            "'" . $eventsAsJson . "'",
            $path . ' >> /app/service/var/logs/' . $this->container->get('kernel')->getEnvironment() . '/publisher.log &'
        ];

        shell_exec(implode(' ', $commnad));
    }

    private function generatePublisherData(Stream $stream, EventIterator $events): array
    {
        $data = [
            'topic' => 'new.events',
            'args' => [
                [
                    'stream' => [
                        'name' => $stream->realName()->toString(),
                    ],
                    'events' => $events->toArray(),
                ]
            ]
        ];

        return $data;
    }
}
