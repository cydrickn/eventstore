<?php

/**
 * This file is part of the cydrickn/event-store library
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Cydrick Nonog <cydrick.dev@gmail.com>
 * @license http://opensource.org/licenses/MIT MIT
 * @link https://github.com/cydrickn/event-store GitHub
 */

declare(strict_types=1);

namespace Cydrickn\EventStore\Application\Command;

use Assert\Assertion;
use Cydrickn\EventStore\Domain\Common\MetaData;
use Cydrickn\EventStore\Domain\Event\Event;
use Cydrickn\EventStore\Domain\Event\EventIterator;
use Cydrickn\EventStore\Domain\Stream\StreamName;
use Cydrickn\EventStore\Infrastructure\Service\WriteService;
use Iterator;

/**
 * Description of AddStreamEvent
 *
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
class AddStreamEventCommand
{
    private $payload;

    public function __construct(array $payload)
    {
        $this->guard($payload);
        $this->payload = $payload;
    }

    public function streamName(): StreamName
    {
        return StreamName::fromString($this->payload['stream']);
    }

    public function expectedVersion(): int
    {
        return $this->payload['expected_version'] ?? WriteService::VERSION_JUST_APPEND;
    }

    public function metaData(): MetaData
    {
        return MetaData::fromArray($this->payload['metadata'] ?? []);
    }

    public function events(): Iterator
    {
        $events = [];

        foreach ($this->payload['events'] as $event) {
            $events[] = Event::fromArray($event);
        }

        return new EventIterator($events);
    }

    public function firstEvent(): Event
    {
        return $this->events()->current();
    }

    public function toArray(): array
    {
        return $this->payload;
    }

    private function guard(array $data): void
    {
        Assertion::keyExists($data, 'stream');
        Assertion::string($data['stream']);
        Assertion::keyExists($data, 'events');
        Assertion::isArray($data['events']);
        Assertion::notEmpty($data['events'], 1);
        foreach ($data['events'] as $event) {
            Assertion::keyExists($event, 'event_id');
            Assertion::uuid($event['event_id']);
            Assertion::keyExists($event, 'event_name');
            Assertion::string($event['event_name']);
            Assertion::keyExists($event, 'event_data');
            Assertion::isArray($event['event_data']);
            Assertion::keyExists($event, 'event_metadata');
            Assertion::isArray($event, 'event_metadata');
            Assertion::keyExists($event, 'event_occured_at');
            Assertion::date($event['event_occured_at'], 'Y-m-d H:i:s');
        }

        if (isset($data['expected_version'])) {
            Assertion::integer($data['expected_version']);
        }

        if (isset($data['metadata'])) {
            Assertion::isArray($data['metadata']);
        }
    }
}
