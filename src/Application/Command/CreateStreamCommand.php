<?php

/**
 * This file is part of the cydrickn/event-store library
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Cydrick Nonog <cydrick.dev@gmail.com>
 * @license http://opensource.org/licenses/MIT MIT
 * @link https://github.com/cydrickn/event-store GitHub
 */

declare(strict_types=1);

namespace Cydrickn\EventStore\Application\Command;

use Assert\Assertion;
use Cydrickn\EventStore\Domain\Common\MetaData;
use Cydrickn\EventStore\Domain\Stream\StreamCategory;
use Cydrickn\EventStore\Domain\Stream\StreamId;
use Cydrickn\EventStore\Domain\Stream\StreamName;

/**
 * Description of CreateCommand
 *
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
class CreateStreamCommand
{
    private $payload;

    public static function fromJsonString(string $json): CreateStreamCommand
    {
        Assertion::isJsonString($json);

        return static::fromArray(json_encode($json, true));
    }

    public function __construct(array $payload)
    {
        $this->guard($payload);
        $this->payload = $payload;
    }

    public function streamId(): StreamId
    {
        return StreamId::fromString($this->payload['stream_id']);
    }

    public function streamName(): StreamName
    {
        return StreamName::fromString($this->payload['stream_name']);
    }

    public function streamMetaData(): MetaData
    {
        return MetaData::fromArray($this->payload['stream_metadata']);
    }

    public function streamCategory(): StreamCategory
    {
        return StreamCategory::fromString($this->payload['category']);
    }

    public function toArray(): array
    {
        return $this->payload;
    }

    private function guard(array $data): void
    {
        Assertion::keyExists($data, 'stream_id');
        Assertion::uuid($data['stream_id']);
        Assertion::keyExists($data, 'stream_name');
        Assertion::string($data['stream_name']);
        Assertion::keyExists($data, 'stream_metadata');
        Assertion::isArray($data['stream_metadata']);
        Assertion::keyExists($data, 'stream_category');
        Assertion::string('stream_category');
    }
}
