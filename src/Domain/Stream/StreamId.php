<?php

/**
 * This file is part of the cydrickn/event-store library
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Cydrick Nonog <cydrick.dev@gmail.com>
 * @license http://opensource.org/licenses/MIT MIT
 * @link https://github.com/cydrickn/event-store GitHub
 */

declare(strict_types=1);

namespace Cydrickn\EventStore\Domain\Stream;

/**
 * Description of StreamId
 *
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
class StreamId
{
    private $uuid;

    public static function generate(): StreamId
    {
        return new StreamId(\Ramsey\Uuid\Uuid::uuid4());
    }

    public static function fromString(string $uuid): StreamId
    {
        return new StreamId(\Ramsey\Uuid\Uuid::fromString($uuid));
    }

    private function __construct(\Ramsey\Uuid\UuidInterface $uuid)
    {
        $this->uuid = $uuid;
    }

    public function toString(): string
    {
        return $this->uuid->toString();
    }
}
