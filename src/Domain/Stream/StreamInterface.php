<?php

/**
 * This file is part of the cydrickn/event-store library
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Cydrick Nonog <cydrick.dev@gmail.com>
 * @license http://opensource.org/licenses/MIT MIT
 * @link https://github.com/cydrickn/event-store GitHub
 */

declare(strict_types=1);

namespace Cydrickn\EventStore\Domain\Stream;

/**
 *
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
interface StreamInterface
{
}
