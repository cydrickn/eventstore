<?php

/**
 * This file is part of the cydrickn/event-store library
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Cydrick Nonog <cydrick.dev@gmail.com>
 * @license http://opensource.org/licenses/MIT MIT
 * @link https://github.com/cydrickn/event-store GitHub
 */

declare(strict_types=1);

namespace Cydrickn\EventStore\Domain\Stream;

use Assert\Assertion;
use Cydrickn\EventStore\Domain\Common\MetaData;
use Serializable;

/**
 * Description of Stream
 *
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
class Stream implements StreamInterface, Serializable
{
    private $id;
    private $name;
    private $realName;
    private $metaData;
    private $category;

    public static function fromJson(string $json): Stream
    {
        $stream = new Stream();
        $stream->unserialize($json);

        return $stream;
    }

    public static function fromArray(array $data): Stream
    {
        $stream = new Stream();
        $stream->guard($data);

        $stream->id = StreamId::fromString($data['stream_id']);
        $stream->name = StreamName::fromString($data['stream_name']);
        $stream->realName = StreamName::fromString($data['stream_real_name']);
        $stream->metaData = MetaData::fromArray($data['stream_metadata']);
        $stream->category = StreamCategory::fromString($data['stream_category']);

        return $stream;
    }

    private function __construct()
    {
    }

    public function id(): StreamId
    {
        return $this->id;
    }

    public function name(): StreamName
    {
        return $this->name;
    }

    public function realName(): StreamName
    {
        return $this->realName;
    }

    public function metaData(): MetaData
    {
        return $this->metaData;
    }

    public function category(): StreamCategory
    {
        return $this->category;
    }

    private function guard(array $data): void
    {
        Assertion::keyExists($data, 'stream_id');
        Assertion::string($data['stream_id']);
        Assertion::keyExists($data, 'stream_name');
        Assertion::string($data['stream_name']);
        Assertion::keyExists($data, 'stream_real_name');
        Assertion::string($data['stream_real_name']);
        Assertion::keyExists($data, 'stream_metadata');
        Assertion::isArray($data['stream_metadata']);
        Assertion::keyExists($data, 'stream_category');
        Assertion::string($data['stream_category']);
    }

    public function serialize(): string
    {
        return json_encode([
            'stream_id' => $this->id()->toString(),
            'stream_name' => $this->name()->toString(),
            'stream_real_name' => $this->table()->toString(),
            'stream_metadata' => $this->metaData()->toArray(),
            'stream_category' => $this->category()->toString(),
        ]);
    }

    public function unserialize($serialized): void
    {
        Assertion::isJsonString($serialized);
        $data = json_decode($serialized, true);
        $this->guard($data);

        $this->id = StreamId::fromString($data['stream_id']);
        $this->name = StreamName::fromString($data['stream_name']);
        $this->realName = StreamName::fromString($data['stream_real_name']);
        $this->metaData = MetaData::fromArray($data['stream_metadata']);
        $this->category = StreamCategory::fromString($data['stream_category']);
    }
}
