<?php

/**
 * This file is part of the cydrickn/event-store library
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Cydrick Nonog <cydrick.dev@gmail.com>
 * @license http://opensource.org/licenses/MIT MIT
 * @link https://github.com/cydrickn/event-store GitHub
 */

declare(strict_types=1);

namespace Cydrickn\EventStore\Domain\Stream;

/**
 * Description of StreamName
 *
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
class StreamName
{
    private $name;

    public static function generate(): StreamName
    {
        
    }

    public static function fromString(string $name): StreamName
    {
        return new StreamName($name);
    }

    private function __construct(string $name)
    {
        $this->name =  $name;
    }

    public function toString(): string
    {
        return $this->name;
    }
}
