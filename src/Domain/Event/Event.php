<?php

/**
 * This file is part of the cydrickn/event-store library
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Cydrick Nonog <cydrick.dev@gmail.com>
 * @license http://opensource.org/licenses/MIT MIT
 * @link https://github.com/cydrickn/event-store GitHub
 */

declare(strict_types=1);

namespace Cydrickn\EventStore\Domain\Event;

use Assert\Assertion;
use Cydrickn\EventStore\Domain\Common\Data;
use Cydrickn\EventStore\Domain\Common\MetaData;
use DateTimeImmutable;
use DateTimeZone;
use Serializable;

/**
 * Description of Event
 *
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
class Event implements Serializable, EventInterface
{
    private $eventId;
    private $eventName;
    private $data;
    private $metaData;
    private $occuredAt;

    public static function fromJson(string $data): Event
    {
        $event = new Event();
        $event->unserialize($data);

        return $event;
    }

    public static function fromArray(array $data): Event
    {
        $event = new Event();
        $event->guard($data);
        $event->eventId = EventId::fromString($data['event_id']);
        $event->eventName = EventName::fromString($data['event_name']);
        $event->data = Data::fromArray($data['event_data']);
        $event->metaData = MetaData::fromArray($data['event_metadata']);
        $event->occuredAt = new \DateTimeImmutable($data['event_occured_at'], new DateTimeZone('UTC'));

        return $event;
    }

    private function __construct()
    {
    }

    public function eventId(): EventId
    {
        return $this->eventId;
    }

    public function id(): EventId
    {
        return $this->eventId;
    }

    public function name(): EventName
    {
        return $this->eventName;
    }

    public function eventName(): EventName
    {
        return $this->eventName;
    }

    public function data(): Data
    {
        return $this->data;
    }

    public function metaData(): MetaData
    {
        return $this->metaData;
    }

    public function occuredAt(): DateTimeImmutable
    {
        return $this->occuredAt;
    }

    public function occuredAtAsString(): string
    {
        return $this->occuredAt()->format('Y-m-d H:i:s');
    }

    public function changeMetaData(MetaData $metaData): Event
    {
        $event = clone $this;
        $event->metaData = $metaData;

        return $event;
    }

    public function setPosition(int $position): void
    {
        $this->metaData()->set('_position', $position);
    }

    public function setVersion(int $version): void
    {
        $this->metaData()->set('_version', $version);
    }

    public function serialize(): string
    {
        return json_encode($this->toArray());
    }

    public function toArray(): array
    {
        return [
            'event_id' => $this->eventId()->toString(),
            'event_name' => $this->eventName(),
            'event_data' => $this->data()->toArray(),
            'event_metadata' => $this->metaData()->toArray(),
            'event_occured_at' => $this->occuredAtAsString(),
        ];
    }

    public function unserialize($serialized): void
    {
        Assertion::isJsonString($serialized);
        $data = json_decode($serialized, true);
        $this->guard($data);

        $this->eventId = EventId::fromString($data['event_id']);
        $this->eventName = EventName::fromString($data['event_name']);
        $this->data = Data::fromArray($data['event_data']);
        $this->metaData = MetaData::fromArray($data['event_metadata']);
        $this->occuredAt = new \DateTimeImmutable($data['event_occured_at'], new DateTimeZone('UTC'));
    }

    private function guard(array $data): void
    {
        Assertion::keyExists($data, 'event_id');
        Assertion::string($data['event_id']);
        Assertion::uuid($data['event_id']);
        Assertion::keyExists($data, 'event_name');
        Assertion::string($data['event_name']);
        Assertion::keyExists($data, 'event_data');
        Assertion::isArray($data['event_data']);
        Assertion::keyExists($data, 'event_metadata');
        Assertion::isArray($data['event_metadata']);
        Assertion::keyExists($data, 'event_occured_at');
        Assertion::date($data['event_occured_at'], 'Y-m-d H:i:s');
    }
}
