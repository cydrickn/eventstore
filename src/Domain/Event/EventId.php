<?php

/**
 * This file is part of the cydrickn/event-store library
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Cydrick Nonog <cydrick.dev@gmail.com>
 * @license http://opensource.org/licenses/MIT MIT
 * @link https://github.com/cydrickn/event-store GitHub
 */

declare(strict_types=1);

namespace Cydrickn\EventStore\Domain\Event;

use Cydrickn\EventStore\Domain\Event\EventId;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * Description of EventId
 *
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
class EventId
{
    private $uuid;

    public static function generate(): EventId
    {
        return new EventId(Uuid::uuid4());
    }

    public static function fromString(string $eventId): EventId
    {
        return new EventId(Uuid::fromString($eventId));
    }

    private function __construct(UuidInterface $uuid)
    {
        $this->uuid = $uuid;
    }

    public function toString(): string
    {
        return $this->uuid->toString();
    }
}
