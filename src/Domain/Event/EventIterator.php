<?php

/**
 * This file is part of the cydrickn/event-store library
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Cydrick Nonog <cydrick.dev@gmail.com>
 * @license http://opensource.org/licenses/MIT MIT
 * @link https://github.com/cydrickn/event-store GitHub
 */

declare(strict_types=1);

namespace Cydrickn\EventStore\Domain\Event;

use Iterator;

/**
 * Description of EventIterator
 *
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
class EventIterator implements Iterator
{
    private $events = [];
    private $position = 0;

    public function __construct(array $events)
    {
        $this->events = $events;
    }

    public function current(): Event
    {
        return $this->events[$this->position];
    }

    public function key()
    {
        return $this->position;
    }

    public function next(): void
    {
        $this->position += 1;
    }

    public function rewind(): void
    {
        $this->position = 0;
    }

    public function valid(): bool
    {
        return isset($this->events[$this->position]);
    }

    public function toArray(): array
    {
        $array = [];
        foreach ($this->events as $event) {
            $array[] = $event->toArray();
        }

        $this->rewind();

        return $array;
    }
}
