<?php

/**
 * This file is part of the cydrickn/event-store library
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Cydrick Nonog <cydrick.dev@gmail.com>
 * @license http://opensource.org/licenses/MIT MIT
 * @link https://github.com/cydrickn/event-store GitHub
 */

declare(strict_types=1);

namespace Cydrickn\EventStore;

use Nelmio\CorsBundle\NelmioCorsBundle;
use Symfony\Bundle\DebugBundle\DebugBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Bundle\TwigBundle\TwigBundle;
use Symfony\Bundle\WebProfilerBundle\WebProfilerBundle;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\RouteCollectionBuilder;

/**
 * Description of Kernel
 *
 * @author Cydrick Nonog <cydrick.dev@gmail.com>
 */
class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    protected function configureContainer(ContainerBuilder $container, LoaderInterface $loader)
    {
        $configDir = $this->getConfigDir();
        $loader->load($configDir . '/parameters.yml');
        if (file_exists($configDir . '/config_' . $this->getEnvironment() . '.yml')) {
            $loader->load($configDir . '/config_' . $this->getEnvironment() . '.yml');
        } else {
            $loader->load($configDir . '/config.yml');
        }
    }

    protected function configureRoutes(RouteCollectionBuilder $routes)
    {
        $configDir = $this->getConfigDir();
        if (file_exists($configDir . '/routes_' . $this->getEnvironment() . '.yml')) {
            $routes->import($configDir . '/routes_' . $this->getEnvironment() . '.yml', '/');
        }
        $routes->import($configDir . '/routes.yml', '/');
    }

    public function registerBundles()
    {
        $bundles = [
            new FrameworkBundle(),
            new NelmioCorsBundle(),
        ];

        if ($this->getEnvironment() === 'dev') {
            $bundles[] = new DebugBundle();
            $bundles[] = new WebProfilerBundle();
            $bundles[] = new TwigBundle();
        }

        return $bundles;
    }

    public function getConfigDir(): string
    {
        return $this->getProjectDir() . '/config';
    }

    public function getCacheDir()
    {
        return $this->getProjectDir() . '/var/cache/' . $this->environment;
    }

    public function getLogDir()
    {
        return $this->getProjectDir() . '/var/logs/' . $this->environment;
    }
}
